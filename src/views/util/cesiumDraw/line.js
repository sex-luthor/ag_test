/*
 * @Author: fisher
 * @Date: 2023-07-06 17:51:05
 * @LastEditTime: 2023-12-14 17:09:30
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @Description: 
 * @FilePath: \front-end-template\src\modules\Digital\utils\cesiumDraw\line.js
 */
import { screen2degrees, GenerateId } from './utils.js'
/* eslint-disable */
export default (handler, collection, features, currentAction, options) => {
  const width = options.width || 5
  const color = options.color || '#e74c3c'
  const line_type = options.line_type || 0
  const opacity = options.opacity || 1
  const elevation = options.elevation || 0
  let points = []
  let coordinates = []
  let tempPoint = []
  let tempLine = undefined
  let _primitives;
  const leftClickCb = (e) => {
    const point = screen2degrees(e.position)
    points.push(point.x, point.y)
    coordinates.push([point.x, point.y, elevation])
    if (!_primitives) {
      _primitives = new Cesium.PrimitiveCollection();
      scene.primitives.add(_primitives);
    }
    // 当有第一个点后开启移动
    if (points.length == 2) {
      handler.setInputAction(mouseMoveCb, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
      // if (!tempLine) {
      //   draw_dynamic()
      // }
    }
  }
  const mouseMoveCb = (e) => {
    tempPoint = screen2degrees(e.endPosition)
    // if (points.length >= 6) {
    //   points.length = points.length - 3
    // }
    if(points.length!=2) points.splice(-2);
    points.push(tempPoint.x, tempPoint.y);
    draw_dynamic();
  }
  const rightClickCb = (e) => {
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    // 去掉最后一个未选择的点
    points.length = points.length - 3
    draw_polyline()
    // 重置动态线，以及静态线初始值
    collection.entities.remove(tempLine)
    tempPoint = []
    tempLine = undefined
    points = []
    coordinates = []
  }
  //绘制动态线
  const draw_dynamic = () => {
    // tempLine = collection.entities.add({
    //   name: 'tempLine',
    //   polyline: {
    //     // 这个方法上面有重点说明
    //     positions: new Cesium.CallbackProperty(() => {
    //       return new Cesium.Cartesian3.fromDegreesArrayHeights(points);
    //     }, false),
    //     // 宽度
    //     width: width,
    //     // 线的颜色
    //     material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
    //   }
    // });
    _primitives.removeAll()
    //相关属性
    let options = {
      allowPicking: true,
      releaseGeometryInstances: false, //保留顶点
      geometryInstances: new Cesium.GeometryInstance({
        geometry: new Cesium.GroundPolylineGeometry({//贴地线几何
          positions:Cesium.Cartesian3.fromDegreesArray(points)
        }),
        attributes: {
          color: Cesium.ColorGeometryInstanceAttribute.fromColor(
            Cesium.Color.ORANGE
          ),
        },
      }),
      appearance: new Cesium.PerInstanceColorAppearance({
        translucent: false,
        closed: true,
      }),

      asynchronous: false,
    };
    let primitive = new Cesium.GroundPolylinePrimitive(options);
    //自定义一个原始参照旋转度
    primitive.rotation = { x: 0, y: 0, z: 0 };
    _primitives.add(primitive);
  }

  //绘制结果线
  const draw_polyline = () => {
    console.log(points);
    const id = GenerateId(13)
    if (points.length >= 3) {
      // 新增cesium 实体
      collection.entities.add({
        id: id,
        name: 'staticLine',
        polyline: {
          positions: new Cesium.Cartesian3.fromDegreesArray(points),
          // 宽度
          width: width,
          // 线的颜色
          material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
        }
      });
      // 新增feature数据
      const feature = {
        'type': 'Feature',
        'properties': {
          'type': 'polyline',
          'id': id,
          'width': width,
          'color': color,
          'line_type': line_type,
          'opacity': opacity,
          'elevation': elevation
        },
        'geometry': {
          'type': 'LineString',
          'coordinates': coordinates
        }
      }
      features.push(feature)
      currentAction && currentAction(feature)
    } else {
      return
    }
  }
  handler.setInputAction(leftClickCb, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  handler.setInputAction(rightClickCb, Cesium.ScreenSpaceEventType.RIGHT_CLICK)

}


