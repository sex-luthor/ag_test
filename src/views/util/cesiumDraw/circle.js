/*
 * @Author: fisher
 * @Date: 2023-07-06 17:51:05
 * @LastEditTime: 2023-07-13 17:34:15
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \jiang_vue\src\views\digital_city\components\center\components\tools\utils\cesiumDraw\circle.js
 */
import { screen2degrees, two_points_distance, GenerateId } from './utils.js'
/* eslint-disable */
export default (handler, collection, features, currentAction) => {
    const fill_color = '#e74c3c'
    const outline_width = 5
    const outline_color = '#34495e'
    let center_point = undefined
    let temp_point = undefined
    let distance = 0
    let tempEllipse = undefined
    let coordinates = []
    const leftClickCb = (e) => {
        if (!center_point) {
            center_point = screen2degrees(e.position)
            coordinates = [center_point.x, center_point.y]
            handler.setInputAction(mouseMoveCb, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
            if (!tempEllipse) {
                draw_dynamic()
            }
        }
    }
    const mouseMoveCb = (e) => {
        temp_point = screen2degrees(e.endPosition)
        distance = two_points_distance(center_point, temp_point)
    }
    const rightClickCb = (e) => {
        handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
        draw_ellipse()
        // // 重置动态线，以及静态线初始值
        collection.entities.remove(tempEllipse)
        temp_point = undefined
        tempEllipse = undefined
        center_point = undefined
        distance = 0
        coordinates = []
    }
    //绘制动态线
    const draw_dynamic = () => {
        tempEllipse = collection.entities.add({
            name: 'tempEllipse',
            position: Cesium.Cartesian3.fromDegrees(center_point.x, center_point.y),
            ellipse: {
                // 半短轴（画圆：半短轴和半长轴一致即可）
                semiMinorAxis: new Cesium.CallbackProperty(() => {
                    // PolygonHierarchy 定义多边形及其孔的线性环的层次结构（空间坐标数组）
                    return distance;
                }, false),
                // 半长轴
                semiMajorAxis: new Cesium.CallbackProperty(() => {
                    // PolygonHierarchy 定义多边形及其孔的线性环的层次结构（空间坐标数组）
                    return distance;
                }, false),
                height: 1,
                // 填充色
                material: Cesium.Color.fromCssColorString(fill_color),
                // 是否有边框
                outline: true,
                // 边框颜色
                outlineColor: Cesium.Color.fromCssColorString(outline_color),
                // 边框宽度
                outlineWidth: outline_width

            }
        });
    }

    //绘制结果线
    const draw_ellipse = () => {
        const id = GenerateId(13)
        collection.entities.add({
            id: id,
            name: 'staticEllipse',
            position: Cesium.Cartesian3.fromDegrees(center_point.x, center_point.y),
            ellipse: {
                // 半短轴（画圆：半短轴和半长轴一致即可）
                semiMinorAxis: distance,
                // 半长轴
                semiMajorAxis: distance,
                height: 1,
                // 填充色
                material: Cesium.Color.fromCssColorString(fill_color),
                // 是否有边框
                outline: true,
                // 边框颜色
                outlineColor: Cesium.Color.fromCssColorString(outline_color),
                // 边框宽度
                outlineWidth: outline_width
            }
        });
        // 新增feature数据
        const feature = {
            "type": "Feature",
            "properties": {
                "type": "ellipse",
                "subType": 'Circle',
                "radius": distance,
                "id": id,
                "outline-color": outline_color,
                "outline-width": outline_width,
                "fill-color": fill_color
            },
            "geometry": {
                "type": "Point",
                "coordinates": coordinates
            }
        }
        features.push(feature)
        currentAction && currentAction(feature)

    }
    handler.setInputAction(leftClickCb, Cesium.ScreenSpaceEventType.LEFT_CLICK)
    handler.setInputAction(rightClickCb, Cesium.ScreenSpaceEventType.RIGHT_CLICK)

}


