/*
 * @Author: fisher
 * @Date: 2023-05-15 16:50:45
 * @LastEditTime: 2023-08-17 11:06:14
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \front-end-template\src\modules\Digital\utils\cesiumDraw\index.js
 */
import Line from './line.js'
import POLYGON from './polygon.js'
import CIRCLE from './circle.js'
import RECT from './rect.js'
import IMAGE from './image.js'
import SHADER from './shader.js'
import BLOCK from './block.js'
/* eslint-disable */
export default class CesiumDraw {
  constructor(options) {
    this._currentAction = options?.action || undefined
    this._mode = 0

    this.#init()

  }
  #collection = undefined
  #features = []
  #drawHandler = undefined

  #draw_modes = {
    'STATIC': 0,
    'LINE': 1,
    'POLYGON': 2,
    'CIRCLE': 3,
    'RECT': 4,
    'LABEL': 5,
    'IMAGE': 6,
    'VIDEO': 7,
    'SHADER': 8,
    'BLOCK': 9
  }

  #init() {
    // 初始化鼠标事件
    if (!this.#drawHandler) {
      this.#drawHandler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas)
    }
    // 初始化集合
    if (!this.#collection) {
      this.#collection = new Cesium.CustomDataSource('drawCollection');
    }
    viewer.dataSources.add(this.#collection)
  }
  #reset() {
    this.#collection.entities.removeAll()
    this.#drawHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK)
    this.#drawHandler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_CLICK)
    this.#drawHandler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
  }
  changeMode(mode, options) {
    // 重置全局变量
    this.#reset()
    // 切换模式
    this._mode = this.#draw_modes[mode]
    switch (this._mode) {
      case 0:
        console.log('static');
        break;
      //   绘制线
      case 1:
        /**
         * @description: 绘制实例/实体集合/元素集合/操作完成回调函数
         * @return {*}
         */
        Line(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      //   绘制面
      case 2:
        POLYGON(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      //   绘制圆
      case 3:
        CIRCLE(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      //   绘制矩形
      case 4:
        RECT(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      //   绘制矩形
      case 6:
        IMAGE(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      //   绘制SHADER
      case 8:
        SHADER(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      // 绘制体块
      case 9:
        BLOCK(this.#drawHandler, this.#collection, this.#features, this._currentAction, options)
        break;
      default:
        break;
    }
  }

  // 绘制贴图
  // 绘制贴视频





  // 释放draw实例
  destroy() {
    this.#reset()
    this.#drawHandler = this.#drawHandler && this.#drawHandler.destroy()
    viewer.dataSources.remove(this.#collection)
  }

  // 开始提示
  startPrompt(cb) {
    cb()
  }

  // 过程提示
  processPrompt(cb) {
    cb()
  }

  // 结束提示
  endPrompt(cb) {
    cb()
  }

  // 获取所有feature
  getAllFeatures() {
    return this.#features
  }

  // 获取geojson
  getGeojson() {
    return {
      'type': 'FeatureCollection',
      'features': this.#features
    }
  }

  // 根据id查询绘制feature
  getFeatureById(id) {
    return this.#features.find(item => item.properties.id === id)
  }
  // 根据id获取实体
  getEntityById(id) {
    return this.#collection.entities.getById(id)
  }
  // 根据id删除绘制feature
  deleteFeatureById(id) {
    // 删除features数据
    this.#features = this.#features.filter(item => item.properties.id !== id)
    // 删除cesium实体
    this.#collection.entities.removeById(id)

  }

  // 清理所有feature
  removeAll() {
    this.#collection.entities.removeAll()
    this.#features = []
  }

  // label区域  // 绘制标签
  // 添加label
  addLabel(options) { }
  //通过id删除label
  deleteLabel(id) { }
  // 清除label
  clearLabel() { }

}

