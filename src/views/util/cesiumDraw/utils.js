/*
 * @Author: fisher
 * @Date: 2023-07-06 18:28:47
 * @LastEditTime: 2023-12-14 10:52:00
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @Description: 
 * @FilePath: \jiang_vue\src\views\digital_city\components\center\components\tools\utils\cesiumDraw\utils.js
 */


/**
 * @description: 屏幕坐标转经纬度坐标
 * @param {*} position
 * @return {*}
 */
/* eslint-disable */
export const screen2degrees = (position) => {
    const res = viewer.scene.pickPosition(position);
    //将笛卡尔坐标转化为经纬度坐标
    const cartographic = Cesium.Cartographic.fromCartesian(res);
    const longitude = Cesium.Math.toDegrees(cartographic.longitude);
    const latitude = Cesium.Math.toDegrees(cartographic.latitude);
    const height = cartographic.height;
    return {
        x: longitude,
        y: latitude,
        z: height
    }
}

/**
 * @description: 计算两坐标间距
 * @param {*} p1
 * @param {*} p2
 * @return {*}
 */
export const two_points_distance = (p1, p2) => {
    // 经纬度转换为世界坐标
    const start_position = Cesium.Cartesian3.fromDegrees(p1.x, p1.y, p1.z);
    const end_position = Cesium.Cartesian3.fromDegrees(p2.x, p2.y, p2.z);
    // 返回两个坐标的距离（单位：米）
    return Cesium.Cartesian3.distance(start_position, end_position);
}

/**
 * @description: 生成id
 * @param {*} len
 * @param {*} radix
 * @return {*}
 */
export const GenerateId = (len, radix) => {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    var uuid = [],
        i;
    radix = radix || chars.length;
    if (len) {
        // Compact form
        for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
    } else {
        // rfc4122, version 4 form
        var r;

        // rfc4122 requires these characters
        uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
        uuid[14] = '4';

        // Fill in random data.  At i==19 set the high bits of clock sequence as
        // per rfc4122, sec. 4.1.5
        for (i = 0; i < 36; i++) {
            if (!uuid[i]) {
                r = 0 | Math.random() * 16;
                uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
            }
        }
    }
    return uuid.join('').toLowerCase();
}

/**
 * @description: 
 * @param {*} p1 屏幕坐标
 * @param {*} p2 屏幕左边
 * @return {*}
 */
export const calculateRectPoints = (p1, p2) => {
    const p3 = new Cesium.Cartesian2(p2.x, p1.y)
    const p4 = new Cesium.Cartesian2(p1.x, p2.y)
    const c1 = screen2degrees(p1)
    const c2 = screen2degrees(p2)
    const c3 = screen2degrees(p3)
    const c4 = screen2degrees(p4)
    return [c1.x, c1.y, c3.x, c3.y, c2.x, c2.y, c4.x, c4.y]
}