/*
 * @Author: fisher
 * @Date: 2023-07-06 17:51:05
 * @LastEditTime: 2023-08-18 16:02:48
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \front-end-template\src\modules\Digital\utils\cesiumDraw\block.js
 */
import { screen2degrees, GenerateId } from './utils.js'
/* eslint-disable */
export default (handler, collection, features, currentAction, options) => {
  const color = options.color || '#e74c3c'
  const opacity = options.opacity || 1
  const extruded_height = options.extruded_height || 0
  const elevation = options.elevation || 1
  const outline_width = 5
  const outline_color = '#34495e'
  let points = []
  let tempPoint = []
  let coordinates = []
  let tempPolygon = undefined
  const leftClickCb = (e) => {
    const point = screen2degrees(e.position)
    points.push(point.x, point.y)
    coordinates.push([point.x, point.y])
    // 当有第一个点后开启移动
    if (points.length == 2) {
      handler.setInputAction(mouseMoveCb, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
      if (!tempPolygon) {
        draw_dynamic()
      }
    }
  }
  const mouseMoveCb = (e) => {
    tempPoint = screen2degrees(e.endPosition)
    if (points.length >= 4) {
      points.length = points.length - 2
    }
    points.push(tempPoint.x, tempPoint.y)
  }
  const rightClickCb = (e) => {
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    // 取消最后一个未选择点位坐标tempPoint
    points.length = points.length - 2
    // polygon坐标点封闭,需要首位坐标一致
    coordinates.push([coordinates[0][0], coordinates[0][1]])
    // 处理coordinates的数组层级
    coordinates = [coordinates]
    // 绘制结果
    draw_polygon()
    // 重置动态线，以及静态线初始值
    collection.entities.remove(tempPolygon)
    tempPolygon = undefined
    tempPoint = []
    points = []
    coordinates = []
  }
  //绘制动态线
  const draw_dynamic = () => {
    tempPolygon = collection.entities.add({
      name: 'tempPolygon',
      polygon: {
        // 这个方法上面有重点说明
        hierarchy: new Cesium.CallbackProperty(() => {
          return new Cesium.Cartesian3.fromDegreesArray(points);
        }, false),
        extrudedHeight: extruded_height,
        height: elevation,
        // 边框
        // outline: true,
        // outlineColor: Cesium.Color.fromCssColorString(outline_color),
        // outlineWidth: outline_width,
        // 填充的颜色
        material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
      }
    });
  }

  //绘制结果线
  const draw_polygon = () => {
    const id = GenerateId(13)
    if (points.length > 4) {
      collection.entities.add({
        id: id,
        name: 'staticPolygon',
        polygon: {
          // Cesium.Cartesian3.fromDegreesArray([经度1, 纬度1, 经度2, 纬度2,])
          // Cesium.Cartesian3.fromDegreesArrayHeights([经度1, 纬度1, 高度1, 经度2, 纬度2, 高度2])
          hierarchy: new Cesium.Cartesian3.fromDegreesArray(points),
          extrudedHeight: extruded_height,
          height: elevation,
          // // 边框
          // outline: true,
          // outlineColor: Cesium.Color.fromCssColorString(outline_color),
          // outlineWidth: outline_width,
          // 线的颜色
          material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
        }
      });
      // 新增feature数据
      const feature = {
        'type': 'Feature',
        'properties': {
          'type': 'block',
          'id': id,
          'outline-color': outline_color,
          'outline-width': outline_width,
          'color': color,
          'opacity': opacity,
          'extruded_height': extruded_height,
          'elevation': elevation
        },
        'geometry': {
          'type': 'Polygon',
          'coordinates': coordinates
        }
      }
      features.push(feature)
      currentAction && currentAction(feature)
    } else {
      return
    }
  }
  handler.setInputAction(leftClickCb, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  handler.setInputAction(rightClickCb, Cesium.ScreenSpaceEventType.RIGHT_CLICK)

}


