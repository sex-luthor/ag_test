/*
 * @Author: fisher
 * @Date: 2023-07-06 17:51:05
 * @LastEditTime: 2023-08-30 17:22:56
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \front-end-template\src\modules\Digital\utils\cesiumDraw\rect.js
 */
/* eslint-disable */
import { screen2degrees, GenerateId, calculateRectPoints } from './utils.js'
export default (handler, collection, features, currentAction, options) => {
  const color = options.color || '#e74c3c'
  const opacity = options.opacity || 1
  const elevation = options.elevation || 1
  const outline_width = 5
  const outline_color = '#34495e'
  let points = []
  let start_point = undefined
  let end_point = undefined
  let tempRect = undefined
  let coordinates = []
  const leftClickCb = (e) => {
    if (!start_point) {
      start_point = e.position.clone()
      handler.setInputAction(mouseMoveCb, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
      if (!tempRect) {
        draw_dynamic()
      }
    }
  }
  const mouseMoveCb = (e) => {
    end_point = e.endPosition.clone()
    points = calculateRectPoints(start_point, end_point)
  }
  const rightClickCb = (e) => {
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    points = calculateRectPoints(start_point, end_point)
    coordinates = [[[points[0], points[1]], [points[2], points[3]], [points[4], points[5]], [points[6], points[7]], [points[0], points[1]]]]
    draw_rect()
    // 重置动态线，以及静态线初始值
    collection.entities.remove(tempRect)
    start_point = undefined
    end_point = undefined
    tempRect = undefined
    points = []
    coordinates = []
  }
  //绘制动态线
  const draw_dynamic = () => {
    tempRect = collection.entities.add({
      name: 'tempRect',
      polygon: {
        // 这个方法上面有重点说明
        hierarchy: new Cesium.CallbackProperty(() => {
          return new Cesium.Cartesian3.fromDegreesArray(points);
        }, false),
        height: elevation,
        // 边框
        // outline: true,
        // outlineColor: Cesium.Color.fromCssColorString(outline_color),
        // outlineWidth: outline_width,
        // 线的颜色
        material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
      }
    });
  }

  //绘制结果线
  const draw_rect = () => {
    const id = GenerateId(13)
    collection.entities.add({
      id: id,
      name: 'staticRect',
      polygon: {
        // Cesium.Cartesian3.fromDegreesArray([经度1, 纬度1, 经度2, 纬度2,])
        // Cesium.Cartesian3.fromDegreesArrayHeights([经度1, 纬度1, 高度1, 经度2, 纬度2, 高度2])
        hierarchy: new Cesium.Cartesian3.fromDegreesArray(points),
        height: elevation,
        // 边框
        // outline: true,
        // outlineColor: Cesium.Color.fromCssColorString(outline_color),
        // outlineWidth: outline_width,
        // 线的颜色
        material: Cesium.Color.fromCssColorString(color).withAlpha(opacity),
      }
    });
    // 新增feature数据
    const feature = {
      'type': 'Feature',
      'properties': {
        'type': 'rect',
        'id': id,
        'outline-color': outline_color,
        'outline-width': outline_width,
        'color': color,
        'opacity': opacity,
        'elevation': elevation
      },
      'geometry': {
        'type': 'Polygon',
        'coordinates': coordinates
      }
    }
    features.push(feature)
    currentAction && currentAction(feature)
  }
  handler.setInputAction(leftClickCb, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  handler.setInputAction(rightClickCb, Cesium.ScreenSpaceEventType.RIGHT_CLICK)

}


