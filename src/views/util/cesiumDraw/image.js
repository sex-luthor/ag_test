/*
 * @Author: fisher
 * @Date: 2023-07-06 17:51:05
 * @LastEditTime: 2023-08-25 19:08:14
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \front-end-template\src\modules\Digital\utils\cesiumDraw\image.js
 */
/* eslint-disable */
import { screen2degrees, GenerateId, calculateRectPoints } from './utils.js'
// import img1 from './assets/imgs/img.json'
export default (handler, collection, features, currentAction, options) => {
  const url = options.url
  const opacity = options.opacity || 1
  const elevation = options.elevation || 1
  const fill_color = '#e74c3c'
  // const outline_width = 5
  const outline_color = '#34495e'
  let points = []
  let start_point = undefined
  let end_point = undefined
  let tempRect = undefined
  let coordinates = []
  const leftClickCb = (e) => {
    if (!start_point) {
      start_point = e.position.clone()
      handler.setInputAction(mouseMoveCb, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
      if (!tempRect) {
        draw_dynamic()
      }
    }
  }
  const mouseMoveCb = (e) => {
    end_point = e.endPosition.clone()
    points = calculateRectPoints(start_point, end_point)
  }
  const rightClickCb = (e) => {
    handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    points = calculateRectPoints(start_point, end_point)
    coordinates = [[[points[0], points[1]], [points[2], points[3]], [points[4], points[5]], [points[6], points[7]], [points[0], points[1]]]]
    draw_rect()
    // 重置动态线，以及静态线初始值
    collection.entities.remove(tempRect)
    start_point = undefined
    end_point = undefined
    tempRect = undefined
    points = []
    coordinates = []
  }
  //绘制动态线
  const draw_dynamic = () => {
    tempRect = collection.entities.add({
      name: 'tempRect',
      polygon: {
        // 这个方法上面有重点说明
        hierarchy: new Cesium.CallbackProperty(() => {
          return new Cesium.Cartesian3.fromDegreesArray(points);
        }, false),
        height: elevation,
        // 边框
        outline: true,
        outlineColor: Cesium.Color.fromCssColorString(outline_color),
        // outlineWidth: outline_width,
        // 线的颜色
        material: Cesium.Color.fromCssColorString(fill_color),
      }
    });
  }

  //绘制结果线
  const draw_rect = () => {
    // // Create video element
    // const video = document.createElement('video');// Use local file
    // // video.src = 'video.mp4';// Use remote file
    // video.src =
    //     'videoPreview/2023-07/c9d87fb20dd42aa4788fc1ac4448c617.mp4';
    // video.controls = true;
    // video.autoplay = true
    // video.loop = true;
    // video.height = 400; // in px
    // video.width = 500; // in pxconst box = document.getElementById('box');// Include in HTML as child of #box
    const heading = viewer.scene.camera.heading
    const id = GenerateId(13)
    collection.entities.add({
      id: id,
      name: 'staticRect',
      polygon: {
        // Cesium.Cartesian3.fromDegreesArray([经度1, 纬度1, 经度2, 纬度2,])
        // Cesium.Cartesian3.fromDegreesArrayHeights([经度1, 纬度1, 高度1, 经度2, 纬度2, 高度2])
        hierarchy: new Cesium.Cartesian3.fromDegreesArray(points),
        height: elevation,
        // 边框
        // outline: true,
        // outlineColor: Cesium.Color.fromCssColorString(outline_color),
        // outlineWidth: outline_width,
        // 线的颜色
        // 视频
        // material: video,
        // 图片
        material: new Cesium.ImageMaterialProperty({
          repeat: Cesium.Cartesian2(1, 1),
          image: url,
          transparent: opacity
        }),
        stRotation: heading
      }
    });
    // 新增feature数据
    const feature = {
      'type': 'Feature',
      'properties': {
        'type': 'image',
        'id': id,
        // 'outline-color': outline_color,
        // 'outline-width': outline_width,
        // 'fill-color': fill_color,
        'url': url,
        'opacity': opacity,
        'color': '#ffffff',
        'rotation': heading,
        'elevation': elevation
      },
      'geometry': {
        'type': 'Polygon',
        'coordinates': coordinates
      }
    }
    features.push(feature)
    currentAction && currentAction(feature)
  }
  handler.setInputAction(leftClickCb, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  handler.setInputAction(rightClickCb, Cesium.ScreenSpaceEventType.RIGHT_CLICK)

}


