/*
 * @Author: lbz_dev 1982212843@qq.com
 * @Date: 2023-11-27 17:13:29
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @LastEditTime: 2023-12-04 16:15:40
 * @FilePath: \ag_test\src\views\layerManager\other\draggingElement.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/* eslint-disable */
export const Dragging = (elementId) => {
  const dragElement = document.getElementById(elementId);
  // this.clickElement = document.getElementById("header");
  // this.clickElement.addEventListener("mousedown", this.onMouseDown);
  // this.clickElement.addEventListener("mouseup", this.onMouseUp);
  // this.clickElement.addEventListener("mousemove", this.onMouseMove);
  let moveElement = document.getElementById("moveElement");
  dragElement.mouseDown = (e) => {
    // X,Y为计算的偏移量，就是鼠标点击的位置相对于元素左上角圆点的距离
    let X = e.clientX - dragElement.offsetLeft;
    let Y = e.clientY - dragElement.offsetTop;
    console.log("XY", X, Y);
    const move = (e) => {
      // 0 < left && left < window.innerWidth - el.offsetWidth 保证移动过程中，元素不超出窗口
      let left = e.clientX - X;
      // if (0 < left && left < window.innerWidth - dragElement.offsetWidth) {
      dragElement.style.left = left + "px";
      // }

      // 0 < top && top < window.innerHeight - el.offsetHeight 保证移动过程中，元素不超出窗口
      let top = e.clientY - Y;
      // if (0 < top && top < window.innerHeight - dragElement.offsetHeight) {
      dragElement.style.top = top + "px";
      // }
    };
    dragElement.addEventListener("mousemove", move);
    moveElement.addEventListener("mouseup", () => {
      dragElement.removeEventListener("mousemove", move);
    });
    moveElement.addEventListener("mouseleave", function () {
      dragElement.removeEventListener("mousemove", move);
    });
  };
  moveElement.addEventListener("mousedown", dragElement.mouseDown);
};
export const leftScaleing = () => {};
let MouseDown;
export const rightScaleing = (el) => {
  const scaleElement = document.getElementById("tree-content");
  const dragElement = document.getElementById(el);
  const layerElement = document.getElementById("layerManager");
  let move;
  MouseDown = (e) => {
    // X,Y为点击位置距离屏幕左上角的长度
    let X = e.clientX;
    let Y = e.clientY;
    let scaleElementHeight = scaleElement.clientHeight;
    // let scaleElementWidth=scaleElement.clientWidth
    let layerElementHeight = layerElement.clientHeight;
    let layerElementWidth = layerElement.clientWidth;
    move = (e) => {
      // 0 < left && left < window.innerWidth - el.offsetWidth 保证移动过程中，元素不超出窗口
      let x = e.clientX - X;
      // if (0 < left && left < window.innerWidth - dragElement.offsetWidth) {
      // scaleElement.style.width =scaleElementWidth +x + 'px';
      layerElement.style.width = layerElementWidth + x + "px";
      // }

      // 0 < top && top < window.innerHeight - el.offsetHeight 保证移动过程中，元素不超出窗口
      let y = e.clientY - Y;
      // if (0 < top && top < window.innerHeight - dragElement.offsetHeight) {
      scaleElement.style.maxHeight = scaleElementHeight + y + "px";
      // layerElement.style.height = layerElementHeight + y + "px";
      // }
    };
    document.addEventListener("mousemove", move);
    dragElement.addEventListener("mouseup", () => {
      document.removeEventListener("mousemove", move);
    });
    dragElement.addEventListener("mouseleave", function () {
      dragElement.removeEventListener("mousemove", move);
    });
  };
  dragElement.addEventListener("mousedown", MouseDown);
};

export class changeEl {
  static MouseDown;
  static dragElement;
  static scaleDragElement
  static widthMouseDown
  static widthDragElement
  static heightDragElement
  static heightMouseDown
  static scaleing = (el) => {
    const scaleElement = document.getElementById("tree-content");
    this.scaleDragElement = document.getElementById(el);
    const layerElement = document.getElementById("layerManager");
    let move;
    this.MouseDown = (e) => {
      // X,Y为点击位置距离屏幕左上角的长度
      let X = e.clientX;
      let Y = e.clientY;
      let scaleElementHeight = scaleElement.clientHeight;
      // let scaleElementWidth=scaleElement.clientWidth
      let layerElementHeight = layerElement.clientHeight;
      let layerElementWidth = layerElement.clientWidth;
      move = (e) => {
        // 0 < left && left < window.innerWidth - el.offsetWidth 保证移动过程中，元素不超出窗口
        let x = e.clientX - X;
        // 0 < top && top < window.innerHeight - el.offsetHeight 保证移动过程中，元素不超出窗口
        let y = e.clientY - Y;
        if(layerElementHeight + y<305||layerElementWidth+x<160||layerElementHeight+y>815||scaleElementHeight + y<=80||scaleElementHeight.clientHeight > scaleElementHeight.scrollHeight) {
          document.removeEventListener('mousemove', move);
          return
        }
        // if (0 < left && left < window.innerWidth - dragElement.offsetWidth) {
        // scaleElement.style.width =scaleElementWidth +x + 'px';
        layerElement.style.width = layerElementWidth + x + "px";
        // }


        // if (0 < top && top < window.innerHeight - dragElement.offsetHeight) {
        scaleElement.style.maxHeight = scaleElementHeight + y + "px";
        // layerElement.style.height = layerElementHeight + y + "px";
        // }
      };
      document.addEventListener("mousemove", move);
      this.scaleDragElement.addEventListener("mouseup", () => {
        document.removeEventListener("mousemove", move);
      });
      document.addEventListener('mouseleave', function() {
        document.removeEventListener('mousemove', move);
      });
    };
    this.scaleDragElement.addEventListener("mousedown", this.MouseDown);
  };
  static removeDown() {
    this.dragElement?.removeEventListener("mousedown", this.MouseDown);
    this.widthDragElement?.removeEventListener("mousedown", this.MouseDown);
  }
  static Dragging = (elementId) => {
    const dragElement = document.getElementById(elementId);
    let moveElement = document.getElementById("moveElement");
    this.MouseDown = (e) => {
      // X,Y为计算的偏移量，就是鼠标点击的位置相对于元素左上角圆点的距离
      let X = e.clientX - dragElement.offsetLeft;
      let Y = e.clientY - dragElement.offsetTop;
      const move = (e) => {
        // 0 < left && left < window.innerWidth - el.offsetWidth 保证移动过程中，元素不超出窗口
        let left = e.clientX - X;
        // if (0 < left && left < window.innerWidth - dragElement.offsetWidth) {
        dragElement.style.left = left + "px";
        // }

        // 0 < top && top < window.innerHeight - el.offsetHeight 保证移动过程中，元素不超出窗口
        let top = e.clientY - Y;
        // if (0 < top && top < window.innerHeight - dragElement.offsetHeight) {
        dragElement.style.top = top + "px";
        // }
      };
      dragElement.addEventListener("mousemove", move);
      moveElement.addEventListener("mouseup", () => {
        dragElement.removeEventListener("mousemove", move);
      });
      moveElement.addEventListener("mouseleave", function () {
        dragElement.removeEventListener("mousemove", move);
      });
    };
    moveElement.addEventListener("mousedown", this.MouseDown);
  };
  static changeWidth(el){
    this.widthDragElement = document.getElementById(el);
    const layerElement = document.getElementById("layerManager");
    let move;
    this.widthMouseDown = (e) => {
      // X,Y为点击位置距离屏幕左上角的长度
      let X = e.clientX;
      let layerElementWidth = layerElement.clientWidth;
      move = (e) => {
        let x = e.clientX - X;
        if(layerElementWidth + x<160) {
          document.removeEventListener('mousemove', move);
          return
        }
        layerElement.style.width = layerElementWidth + x + "px";
      };
      document.addEventListener("mousemove", move);
      this.widthDragElement.addEventListener("mouseup", () => {
        document.removeEventListener("mousemove", move);
      });
      document.addEventListener('mouseleave', function() {
        document.removeEventListener('mousemove', move);
      });
    };
    this.widthDragElement.addEventListener("mousedown", this.widthMouseDown);
  }
  static changeHeigth(el){
    this.heightDragElement = document.getElementById(el);
    const scaleElement = document.getElementById("tree-content");
    const layerElement = document.getElementById("layerManager");
    let move;
    this.heightMouseDown = (e) => {
      // X,Y为点击位置距离屏幕左上角的长度
      let Y = e.clientY;
      let layerElementHeight = layerElement.clientHeight;
      let scaleElementHeight = scaleElement.clientHeight;
      move = (e) => {
        let y = e.clientY - Y;
        if(layerElementHeight + y<305||layerElementHeight+y>815) {
          document.removeEventListener('mousemove', move);
          return
        }
        // layerElement.style.height = layerElementHeight + y + "px";
        if(scaleElementHeight + y>=80) scaleElement.style.maxHeight = scaleElementHeight + y + "px";
      };
      document.addEventListener("mousemove", move);
      document.addEventListener("mouseup", () => {
        document.removeEventListener("mousemove", move);
      });
      document.addEventListener('mouseleave', () =>{
        document.removeEventListener('mousemove', move);
      });
    };
    this.heightDragElement.addEventListener("mousedown", this.heightMouseDown);
  }
}
