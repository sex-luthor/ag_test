/*
 * @Author: lbz_dev 1982212843@qq.com
 * @Date: 2023-11-29 16:30:13
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @LastEditTime: 2023-12-12 10:43:00
 * @FilePath: \ag_test\src\views\layerManager\util\layerManager.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import store from "@/store/index.js";
class LayerManager {
  static baseHeight=2;
  static addWMTS(layerInfo) {
    let wmts = new Cesium.WebMapTileServiceImageryProvider({
      url: layerInfo.url,
      layer: "World_Imagery",
      style: "default",
      format: "image/jpeg",
      tileMatrixSetID: "default028mm",
      maximumLevel: 23,
      show: true,
    });
    let imageryLayer = viewer?.imageryLayers.addImageryProvider(wmts);
    viewer.imageryLayers.lowerToBottom(imageryLayer);
    if (viewer.imageryLayers._layers[0]._isBaseLayer)
      viewer.imageryLayers.raise(imageryLayer); //如果有默认初始化底图则再将新加图层放在倒数第二层
    store.commit("addWMTSLayerInfo", {
      id: layerInfo.id,
      layerIndex: imageryLayer._layerIndex,
      status: true,
    });
    return imageryLayer;
  }
  static addXYZ(layerInfo) {
    let xyz = new Cesium.UrlTemplateImageryProvider({
      url: layerInfo.url,
      subdomains: ["a", "b", "c"],
      show: true,
    });
    let imageryLayer = viewer?.imageryLayers.addImageryProvider(xyz);
    viewer.imageryLayers.lowerToBottom(imageryLayer);
    if (viewer.imageryLayers._layers[0]._isBaseLayer)
      viewer.imageryLayers.raise(imageryLayer); //如果有默认初始化底图则再将新加图层放在倒数第二层
    store.commit("addWMTSLayerInfo", {
      id: layerInfo.id,
      layerIndex: imageryLayer._layerIndex,
      status: true,
    });
    return imageryLayer;
  }
  static add3dTiles(layerInfo) {
    const tileset = new Cesium.Cesium3DTileset({
      url: layerInfo.url,
    });
    let tilesLayer;
    tileset.readyPromise.then(function (tileset) {
      tilesLayer = viewer.scene.primitives.add(tileset);
      window.tilesLayer = tilesLayer;
      viewer.zoomTo(
        tileset,
        new Cesium.HeadingPitchRange(
          0.0,
          -0.5,
          tileset.boundingSphere.radius * 2.0
        )
      );
      store.commit("addTilesLayers", {
        id: layerInfo.id,
        layerIndex: viewer.scene.primitives._primitives.length - 1,
        status: true,
      });
    });
    return tileset;
  }
  static async addGeoJson(layerInfo) {
    let urlArr = layerInfo.url.split("/");
    // const GeoJsonData = viewer.dataSources.add(
    //   Cesium.GeoJsonDataSource.load(layerInfo.url, {
    //     fill: Cesium.Color.PINK, //填充色
    //     stroke: Cesium.Color.HOTPINK, //轮廓颜色
    //     strokeWidth: 5, //轮廓宽度
    //     clampToGround:false//是否贴地
    //   })
    // );
    Cesium.Math.setRandomNumberSeed(0); //设置随机数种子
    let dataSource = await Cesium.GeoJsonDataSource.load(layerInfo.url); //geojson面数据
    let GeoJsonData;
    // promise.then(function(dataSource) {
    GeoJsonData = await viewer.dataSources.add(dataSource);
    window.GeoJsonData = GeoJsonData;
    // var entities = GeoJsonData.entities.values;
    // var colorHash = {};
    // for (var i = 0; i < entities.length; i++) {
    //   var entity = entities[i];
    //   // var name = entity.name;  //geojson里面必须得有一个name属性，entity.name对应
    //   // var color = colorHash[name]; //可以使两个同名要素使用同一种颜色。。。
    //   // if (!color) {
    //   let color = Cesium.Color.fromRandom({
    //     alpha: 1.0,
    //   });
    //   //     colorHash[name] = color;
    //   // }
    //   // entity.polygon.heigth._value=1000;
    //   console.log("heigth=====", entity.polygon.heigth);
    //   entity.polygon.material = color;
    //   entity.polygon.outline = false;
    //   entity.polygon.extrudedHeight = 0.5; //20000~60000的随机数，单位是米
    // }
    console.log('this.baseHeight',this.baseHeight);
    GeoJsonData.entities.values.forEach((item) => {
      let color = Cesium.Color.fromRandom({
        alpha: 1.0,
      });
      item.polygon.perPositionHeight=false ;
      item.polygon.material = color;
      item.polygon.height._value=this.baseHeight;
      item.polygon.outline = false;
    //   item.polygon.extrudedHeight = 0.5; //20000~60000的随机数，单位是米。如果同时开启extrudedHeight和height属性则只会显示extrudedHeight（polygon自身拉伸高度）
    });
    this.baseHeight+=0.7;
    viewer.zoomTo(GeoJsonData);
    // });
    //   GeoJsonData.then(viewer.zoomTo(GeoJsonData));
    store.commit("addVectorLayers", {
      id: layerInfo.id,
      name: urlArr[urlArr.length - 1],
      //   layerIndex: GeoJsonData._layerIndex,
      status: true,
    });
    return GeoJsonData;
  }
}
export default LayerManager;
