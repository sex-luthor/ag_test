/*
 * @Author: fisher
 * @Date: 2022-11-09 15:04:00
 * @LastEditTime: 2022-11-14 09:39:10
 * @LastEditors: fisher
 * @Description: 
 * @FilePath: \jiang_vue\src\views\digital_city\configs\index.config.js
 */


export const iserverUrl = "http://jgbim.qhfct.com:9999"


// 地图底图定义
const electronicMap = new Cesium.MapboxStyleImageryProvider({
    url: 'https://api.mapbox.com/styles/v1/',
    username: 'fisher',
    styleId: 'ckqpzk8h52u6v19n2685cnwek',
    accessToken: 'pk.eyJ1IjoibS1maXNoZXIiLCJhIjoiY2t2YWw4NnY4MG11OTJ1czE0YXFhanUwdCJ9.wMArjGf0VkOzdtHFcme6eQ',
    credit: 'MapBox',
    rectangle: Cesium.Rectangle.fromDegrees(113.30749511718747, 22.076731960438288, 115.21911621093749, 23.15046202922408)
})
const notesMap = new Cesium.WebMapTileServiceImageryProvider({
    url:
        'http://t0.tianditu.gov.cn/cia_w/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=cia&tileMatrixSet=w&TileMatrix={TileMatrix}&TileRow={TileRow}&TileCol={TileCol}&style=default.jpg&tk=98e5cadb6046ae58140df1c26d8f415b',
    layer: 'tdtAnnoLayer',
    style: 'default',
    format: 'image/jpeg',
    tileMatrixSetID: 'GoogleMapsCompatible',
    maximumLevel: 18,
    show: true
})
const imageMap = new Cesium.WebMapTileServiceImageryProvider({
    url:
        'http://t0.tianditu.gov.cn/img_w/wmts?service=wmts&request=GetTile&version=1.0.0&LAYER=img&tileMatrixSet=w&TileMatrix={TileMatrix}&TileRow={TileRow}&TileCol={TileCol}&style=default&format=tiles&tk=98e5cadb6046ae58140df1c26d8f415b',
    layer: 'tdtBasicLayer',
    style: 'default',
    format: 'image/jpeg',
    tileMatrixSetID: 'GoogleMapsCompatible',
    maximumLevel: 18,
    show: true
})
// const qhImageMap = imageryLayers.addImageryProvider(getOrthophoto());
export {
    electronicMap,
    notesMap,
    imageMap
    // qhImageMap
}
