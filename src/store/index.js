/*
 * @Author: lbz_dev 1982212843@qq.com
 * @Date: 2023-09-18 14:23:01
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @LastEditTime: 2023-11-29 16:58:52
 * @FilePath: \ag_test\src\store\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from "vue";
import Vuex from "vuex";
import layerManager from "./modules/layerManager";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    layerManager,
  },
});
