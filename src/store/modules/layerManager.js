export default {
  state: () => ({
    wmtsLayers: [],
    wmsLayers: [],
    xzyLayers: [],
    tilesLayers: [],
    vectorLayers: [],
  }),
  mutations: {
    updateWMTSLayers(state, val) {
      if (val.isInit) {
        state.wmtsLayers = [];
        return;
      }
      state.wmtsLayers.forEach((item, index) => {
        if (item.id == val.id) {
          state.wmtsLayers[index] = val;
        }
      });
    },
    addWMTSLayerInfo(state, val) {
      state.wmtsLayers.forEach((item) => {
        item.layerIndex++;
      });
      state.wmtsLayers.push(val);
    },
    updateTilesLayers(state, val) {
      if (val.isInit) {
        state.tilesLayers = [];
        return;
      }
      state.tilesLayers.forEach((item, index) => {
        if (item.id == val.id) {
          state.tilesLayers[index] = val;
        }
      });
    },
    addTilesLayers(state, val) {
      state.tilesLayers.push(val);
    },
    updateVectorLayers(state, val) {
      if (val.isInit) {
        state.vectorLayers = [];
        return;
      }
      state.vectorLayers.forEach((item, index) => {
        if (item.id == val.id) {
          state.vectorLayers[index] = val;
        }
      });
    },
    addVectorLayers(state, val) {
      state.vectorLayers.push(val);
    },
    updateXYZLayers(state, val) {
      if (val.isInit) {
        state.xyzLayers = [];
        return;
      }
      state.xyzLayers.forEach((item, index) => {
        if (item.id == val.id) {
          state.xyzLayers[index] = val;
        }
      });
    },
    addXYZLayers(state, val) {
      state.xyzLayers.forEach((item) => {
        item.layerIndex++;
      });
      state.xyzLayers.push(val);
    },
    updateWMSLayers(state, val) {
      if (val.isInit) {
        state.wmsLayers = [];
        return;
      }
      state.wmsLayers.forEach((item, index) => {
        if (item.id == val.id) {
          state.wmsLayers[index] = val;
        }
      });
    },
    addWMSLayers(state, val) {
      state.wmsLayers.push(val);
    },
  },
};
