/*
 * @Author: lbz_dev 1982212843@qq.com
 * @Date: 2023-09-18 14:23:01
 * @LastEditors: lbz_dev 1982212843@qq.com
 * @LastEditTime: 2023-11-27 16:12:48
 * @FilePath: \ag_test\src\main.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// import animated from 'animate.css';
// Vue.use(animated);
Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
